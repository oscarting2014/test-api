const mongoose = require("mongoose")

const weatherSchema = new mongoose.Schema({
  weather: {
    type: String,
    required: true,
    min: 1,
    max: 255,
  },
  date: {
    type: Date,
    default: Date.now,
  },
})

module.exports = mongoose.model("Weather", weatherSchema)