const router = require("express").Router()
const jwt = require("jsonwebtoken")
const bcrypt = require("bcryptjs")

const User = require("../model/User")

// validation
const { registerValidation, loginValidation } = require("../validation")

// register route
router.post("/register", async (req, res) => {
  // validate the user
  const { error } = registerValidation(req.body)

  // throw validation errors
  if (error) return res.status(400).json({ error: error.details[0].message })

  const isNameExist = await User.findOne({ name: req.body.name })

  // throw error when name already registered
  if (isNameExist)
    return res.status(400).json({ error: "Name already exists" })

  // hash the password
  const salt = await bcrypt.genSalt(10)
  const password = await bcrypt.hash(req.body.password, salt)

  const user = new User({
    name: req.body.name,
    password,
  })

  try {
    const savedUser = await user.save()
    res.json({ error: null, data: { userId: savedUser._id } })
  } catch (error) {
    res.status(400).json({ error })
  }
})

// login route
router.post("/login", async (req, res) => {
  // validate the user
  const { error } = loginValidation(req.body)

  // throw validation errors
  if (error) return res.status(400).json({ error: error.details[0].message })

  const user = await User.findOne({ name: req.body.name })

  // throw error when name is wrong
  if (!user) return res.status(400).json({ error: "Name is wrong" })

  // check for password correctness
  const validPassword = await bcrypt.compare(req.body.password, user.password)
  if (!validPassword)
    return res.status(400).json({ error: "Password is wrong" })

  // create token
  const token = jwt.sign(
    // payload data
    {
      name: user.name,
      id: user._id,
    },
    process.env.TOKEN_SECRET
  )

  res.header("auth-token", token).json({
    error: null,
    data: {
      token,
    },
  })
})

module.exports = router