const router = require("express").Router()
const https = require('https')
const Weather = require("../model/Weather")

const options = {
  hostname: 'api.openweathermap.org',
  path: '/data/2.5/weather?q=HongKong&appid=507e3ff5da3d34dacbbc397e076130a1',
  method: 'GET'
}

// dashboard route
router.get("/", (req, res) => {
  const request = https.request(options, respond => {
    respond.on('data', async(d) => {
      var object = JSON.parse(d)
      var weather = new Weather ({
      	weather:object['weather'][0]['main']
      })
      try {
        const savedWeather = await weather.save()
        res.json({ error: null, data: { weather: savedWeather.weather } })
      } catch (error) {
        res.status(400).json({ error })
      }
    })
  })

  request.on('error', async(error) => {
    console.log("OpenWeatherMap is down")
    console.log("Retrive latest weather from db")
    var latestWeather = await Weather.find().sort([['date', 'desc']]).limit(1)

    if (latestWeather) {
      res.json({
        error: null,
        data: {
          weather:latestWeather[0].weather
        },
      })
    } else {
      res.json({
        error: "No data in db"
      })
    }
  })

  request.end()
})

module.exports = router