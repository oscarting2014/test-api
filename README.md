1.  Open Command Prompt
2.  cd test-api/src/
3.  Type `node index.js` to start the server
4.  Install google plugin Postman https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop?hl=en

# Postman tests

5.  Choose "Post" method and type in URL "http://localhost:3000/api/user/register" to register a login account
6.  In Headers, add `Key: "Content-Type"` and `Value: "application/json"`
7.  In Body, choose "raw" to input the body part of post.
    eg.
    `
    {
        "name":"justin",
 	    "password":"123456"
    }
    `
8.  Send the request and if the name doesn't exist, it should return
`
    {
    	"error": null,
    	data": {
       		"userId": "60918a9cb30562098c44d029"
    	}
	}
	Otherwise, it should return
	{
    	"error": "Name already exists"
	}
	`

9.  Open a new tab, choose "Post" method and type in URL "http://localhost:3000/api/user/login" to login
10.	Repeat step 6 and 7
11. Send the request and if login is successful, it should return
	`{
    	"error": null,
    	"data": {
        	"token": "<Token>"
    	}
	}`
	which is a JWT token

	Otherwise, it will return `"Name is wrong"` or `"Password is wrong"`

12.	Open a new tab, choose "Get" method and type in URL "http://localhost:3000/api/test" to get weather info
13. In Headers, add `Key: "auth-token"` and `Value: "<Token>"` taken from step 11
14. Send the request and the weather should be shown.
	If OpenWeatherMap is down, the weather will be retrived from the latest date written in database. 

